﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrodMonitoring.Model
{
	public class ModelOperation
	{
		public uint Bin { get; set; }
		public string Brand { get; set; }
		public string BinType { get; set; }
		public string BinLevel { get; set; }
		public string TypeOperation { get; set; }
		public string Country { get; set; }	
	}
}
