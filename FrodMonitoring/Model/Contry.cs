﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrodMonitoring.Model
{
	public enum Country
	{
		UZB,
		SVK,
		SGP,
		SAU,
		ROU,
		POL,
		MYS,
		LVA,
		ISR,
		GHA,
		FIN,
		ESP,
		DNK,
		CHL,
		BRA,
		BLZ,
		BGR,
		AUS,
		ARG,
		ARE,
		AGO,
		USA,
		UKR,
		TUR,
		RUS,
		MEX,
		KAZ,
		ITA,
		GBR,
		FRA,
		EST,
		EGY,
		DEU,
		CZE,
		CHN,
		CHE,
		BLR,
		AZE,
		AUT,
		ARM
	}
}
