﻿using FrodMonitoring.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FrodMonitoring
{
	public partial class MainWindow : Window
	{
		private uint _binKart = 0;
		private string _brand = String.Empty;
		private string _binType = String.Empty;
		private string _binLevel = String.Empty;
		private string _country = String.Empty;
		private string _typeOperation = String.Empty;

		public MainWindow()
		{
			InitializeComponent();
		}

		private bool Initialize()
		{
			try
			{
				_binKart = Convert.ToUInt32(textBox.Text);
				_brand = comboBox_Brand.Text;
				_binType = comboBox_BinType.Text;
				_binLevel = comboBox_BinLevel.Text;
				_country = comboBox_Country.Text;
				_typeOperation = comboBox_TypeOperation.Text;

				if (_binKart == 0 || _brand == String.Empty || _binType == String.Empty || _binLevel == String.Empty
					|| _country == String.Empty || _typeOperation == String.Empty)
					throw new Exception();

				return false;
			}
			catch (Exception)
			{
				label.Content = "Что то пошло не так, повторите ввод";
				return true;
			}
		}

		private void Button_Click_Credit(object sender, RoutedEventArgs e)
		{
			if (Initialize())
				return;

			if (RuleCreditBinTypeBinLevelCountry(_binType, _binLevel, _country))
				return;

			FormationJson(_binKart, _brand, _binType, _binLevel, _country, _typeOperation);
		}

		private void Button_Click_Debit(object sender, RoutedEventArgs e)
		{
			if (Initialize())
				return;

			if (RuleDebitBinCart(_binKart))
				return;

			if (RuleDebitCartBrandBinLevel(_brand, _binLevel))
				return;

			FormationJson(_binKart, _brand, _binType, _binLevel, _country, _typeOperation);

		}

		/// <summary>
		/// 1.	Запретить пополнение карт, где BIN == 445199
		/// </summary>
		/// <param name="binKart"></param>
		private bool RuleDebitBinCart(uint binKart)
		{
			uint banBinRart = 445199;

			if (binKart == banBinRart)
			{
				label.Content = "Нельзя пополнять курту с таким BIN номером";
				return true;
			}

			label.Content = "Пополнение прошло успешно";
			return false;

		}

		/// <summary>
		/// 2.	Запретить пополнение карт, где BRAND == VISA, BIN_LEVEL == ELECTRON
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="binLevel"></param>
		private bool RuleDebitCartBrandBinLevel(string brandNow, string binLevelNow)
		{
			Brand brand = Brand.VISA;
			BinLevel binLevel = BinLevel.ELECTRON;

			if (brandNow == brand.ToString() && binLevelNow == binLevel.ToString())
			{
				label.Content = "Запрещено пополнять карту VISA ELECTRON";
				return true;
			}

			label.Content = "Пополнение прошло успешно";
			return false;
		}

		/// <summary>
		/// 3.	Запретить списание c карт, где BIN_TYPE = CREDIT, BIN_LEVEL = SIGNATURE, COUNTRY2_ISO != RUS
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="binLevel"></param>
		/// <param name="contry"></param>
		private bool RuleCreditBinTypeBinLevelCountry(string binTypeNow, string binLevelNow, string countryNow)
		{
			BinType binType = BinType.CREDIT;
			BinLevel binLevel = BinLevel.SIGNATURE;
			Country country = Country.RUS;

			if (binTypeNow == binType.ToString() && binLevelNow == binLevel.ToString() && countryNow != country.ToString())
			{
				label.Content = "Запрещено списование с CREDIT карт SIGNATURE, не принадлежавшие РФ";
				return true;
			}

			label.Content = "Списание прошло";
			return false;


		}

		private void FormationJson(uint binKart, string brand, string binType, string binLevel, string coutry, string typeOperation)
		{
			ModelOperation model = new ModelOperation
			{
				Bin = binKart,
				Brand = brand,
				BinType = binType,
				BinLevel = binLevel,
				Country = coutry,
				TypeOperation = typeOperation
			};

			var jsonOperation = new JsonSerializerOptions { WriteIndented = true };
			var json = JsonSerializer.Serialize<ModelOperation>(model, jsonOperation);

			string falePath = String.Format(@".\DataTemp.json");

			using (FileStream fileStream = new FileStream(falePath, FileMode.OpenOrCreate)) { }

			using (StreamWriter streamWriter = new StreamWriter(falePath, true, System.Text.Encoding.UTF8))
			{
				streamWriter.WriteLine(json);
			}
		}

		private void textBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			textBox.MaxLength = 6;
			e.Handled = !(Char.IsDigit(e.Text, 0));
		}

		private void comboBox_Brand_Initialized(object sender, EventArgs e)
		{
			for (Brand brand = Brand.MAESTRO; brand <= Brand.VISA; brand++)
			{
				comboBox_Brand.Items.Add(brand);
			}
		}

		private void comboBox_BinType_Initialized(object sender, EventArgs e)
		{
			for (BinType binType = BinType.CREDIT; binType <= BinType.CREDIT_DEBIT; binType++)
			{
				comboBox_BinType.Items.Add(binType);
			}
		}

		private void comboBox_BinLevel_Initialized(object sender, EventArgs e)
		{
			for (BinLevel binLevel = BinLevel.ELECTRON; binLevel <= BinLevel.WORLD_BUSINESS; binLevel++)
			{
				comboBox_BinLevel.Items.Add(binLevel);
			}
		}

		private void comboBox_Country_Initialized(object sender, EventArgs e)
		{
			for (Country country = Country.UZB; country <= Country.ARM; country++)
			{
				comboBox_Country.Items.Add(country);
			}
		}

		private void comboBox_TypeOperation_Initialized(object sender, EventArgs e)
		{
			for (TypeOperation typeOperation = TypeOperation.Debit; typeOperation <= TypeOperation.Credit; typeOperation++)
			{
				comboBox_TypeOperation.Items.Add(typeOperation);
			}

		}

		private void comboBox_TypeOperation_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (comboBox_TypeOperation.SelectedItem.ToString() == TypeOperation.Debit.ToString())
			{
				button_Debit.Visibility = Visibility.Visible;
				button_Credit.Visibility = Visibility.Hidden;
			}

			if (comboBox_TypeOperation.SelectedItem.ToString() == TypeOperation.Credit.ToString())
			{
				button_Debit.Visibility = Visibility.Hidden;
				button_Credit.Visibility = Visibility.Visible;
			}
		}
	}
}
