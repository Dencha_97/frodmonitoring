﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrodMonitoring.Model
{

	public enum BinLevel
	{
		ELECTRON,
		PLATINUM,
		GOLD,
		STANDARD,
		INFINITE,
		BUSINESS,
		PREPAID,
		BLACK,
		CLASSIC,
		SIGNATURE,
		STANDARD_BUSINESS,
		STANDARD_GOLD,
		WORLD,
		PRIVAT_LABEL,
		CORPORATE_T_E,
		WORLD_BUSINESS
	}

}
